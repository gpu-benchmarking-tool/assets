#version 410
//in highp vec4 vColor;
uniform sampler2D a_texture;
in vec2 uvs;
in vec3 coords;
out highp vec4 fColor;

void main()
{
//   fColor = texture(a_texture, uvs);
   fColor.xyz = normalize(coords);
   fColor.w = 1;
}
