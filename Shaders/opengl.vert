#version 410

uniform mat4 mvp_matrix;

in vec3 position;
in vec2 textureCoordinate;
out vec2 uvs;
out vec3 coords;
//out vec4 vColor;

void main()
{
 gl_Position = mvp_matrix * vec4(position, 1);
 uvs = textureCoordinate;
 coords = position;
//  vColor = vec4(color, 1.0f);
}
